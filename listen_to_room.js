/**
 * Subscribes and listens to all messages happening in a Rocket.Chat Channel/Room
 * When receiving a message with !gif [query] it will search for a gif and send it into the Channel/Room
 *
 * Written by Tobias Kerzmann <development@tkerzmann.de>
 * Git Repo: https://gitlab.com/Grubdragon/rocket-chat-bots.git
 * 
 */
 
const https = require('https');

let server = "rocketchat.pixsoftware.de";
let serverPort = 443;
let authToken = "kinqROOUpVRIMyUDxgmuxa436J40Fg7m7DkwNtNIdOI";
let subscriptions = ["QCzHXxZkeYotqruDX", "iD7hMkG6kiN7HJBQ4", "dLx2TbbSoG7jD9bmr"];


let DDP = require("ddp");
let login = require("ddp-login");
process.env.METEOR_TOKEN = authToken;

let ddpArray = [];

for (let subscribe of subscriptions) {
	ddpArray[subscribe] = new DDP({
		host: server,
		port: serverPort,
		maintainCollections: true
	});

	ddpArray[subscribe].connect(function(err) {
		if (err) throw err;

		login(ddpArray[subscribe], {
			env: "METEOR_TOKEN",
			method: "token",
			retry: 5
		},

		function(error, userInfo) {
			if (error) {
				// Something went wrong... 
			} else {
				// We are now logged in, with userInfo.token as our session auth token. 
				token = userInfo.token;

				ddpArray[subscribe].subscribe("stream-room-messages", [subscribe, false], function() {

					ddpArray[subscribe].on("message", function(msg) {
						let msgJson = JSON.parse(msg);
	 
						if (msgJson.msg === "changed") {
							console.log(subscribe + ":");
							let query = getChatMessage(msgJson);
							
							if (query !== "empty") {
								sendGif(query, subscribe);
							}
						}
					});
				});
			}
		});
	});
}

function getChatMessage(msgJson) {
	console.log("Got: " + msgJson.toString());
	
	let msgFields = msgJson.fields;
	let msgArgs;
	if (msgFields) {
		msgArgs = msgFields.args;
	}
	  
	if (msgArgs) {
		let command = msgArgs[0].msg;
		
		if (command.match('!gif \\w')) {
			console.log("Yo");
			return msgArgs[0].msg;
		} else {
			console.log("no");
			return "empty";
			// send help message
		}
	}
}

function sendGif(query, subscribe) {
	query = query.substring(5);
	query = query.replace(/\s/g, '-');
	console.log("Gif query: " + query);
	
	let options = {
		hostname: "api.giphy.com",
		port: serverPort,
		path: "/v1/gifs/search?api_key=olTVQmtV8VrbTY7qHu7n4MqT0g25oRJw&limit=1&q=" + query,
		method: "GET"
	}
	
	let req = https.request(options, (res) => {
		let responseData = "";
		
		res.on('data', (d) => {
			responseData += d.toString('utf8');
		});
		
		res.on('end', () => {
			let responseJson = JSON.parse(responseData);
			
			let dataJson = responseJson.data;
			
			if (dataJson) {
				let url = dataJson[0].images.original.url;
				
				sendMessage(url, subscribe);
			}
		});
	});
	
	req.on('error', (d) => {
		console.log(d);
	});
	
	req.end();
}

function sendMessage(msg, subscribe) {
	let postData = JSON.stringify({
		'channel': subscribe,
		'text': msg,
		'alias': 'Chat Bot',
		'avatar': 'https://3.bp.blogspot.com/-vO7C5BPCaCQ/WigyjG6Q8lI/AAAAAAAAfyQ/1tobZMMwZ2YEI0zx5De7kD31znbUAth0gCLcBGAs/s200/TOMI_avatar_full.png'
	});
	
	let options = {
		hostname: server,
		port: serverPort,
		path: '/api/v1/chat.postMessage',
		method: 'POST',
		headers: {
			'X-Auth-Token': authToken,
			'X-User-Id': 'ryvSyJKkC4Jdynjn7',
			'Content-Type': 'application/json'
		}
	}
	
	let req = https.request(options, (res) => {

		res.on('data', (d) => {
			process.stdout.write(d);
		});
	});
	
	req.on('error', (e) => {
		console.error(e);
	});

	req.write(postData);
	req.end();
}